# stage_simplon

Dans ce programme nous allons réaliser une application permettant à chaque apprenant de Simplon de pouvoir suivre son parcours de recherche d'entreprise.

Celle-ci permettra également d'avoir accès aux contacts d'entreprises de Simplon avec des règles d'accès restreint afin d'éviter une surdemande et également en suivant les règles du RGPD.

