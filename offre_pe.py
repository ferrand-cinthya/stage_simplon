from offres_emploi import Api
import pandas as pd
import csv #.get
import requests
import sqlite3

def offre_ent():
    base = {'intitule':[],'description':[],'dateCreation':[],
        'dateActualisatoin':[],'lieu':[],'codePostal':[],
        'entreprise':[],'typeContrat':[],'experienceLibelle':[],
        'altenance':[],'accessibleTH':[],'origineOffre':[]
       }
    client = Api(client_id="PAR_suivirechechedestagec_19efb543c22da989b4d0a28181ebd3d92a94f02a49e6769b8d87608a1fcc8704", 
             client_secret="c09c885d8f49546c18a687df2ddd271f6e220f56cfceb0e00e359662e60c1ead")

    params = {"motsCles": "informatique",
              "sort" : 1
             }
    basic_search = client.search(params=params)
    for i in range(len(basic_search['resultats'])):
        base["intitule"].append(basic_search['resultats'][i]['intitule'])
        base["description"].append(basic_search['resultats'][i]['description'])
        base["dateCreation"].append(basic_search['resultats'][i]['dateCreation'])
        base["lieu"].append(basic_search['resultats'][i]['lieuTravail']['libelle'])
        base["codePostal"].append(basic_search['resultats'][i]['lieuTravail']['codePostal'])
        base["entreprise"].append(basic_search['resultats'][i]['entreprise'])
        if "nom" in basic_search['resultats'][i]['entreprise']:
            base["entreprise"].append(basic_search['resultats'][i]['entreprise']["nom"])
        else:
            base["entreprise"].append("Unknow")
        #base["typeContract"].append(basic_search['resultats'][i]['typeContract'])
        base["experienceLibelle"].append(basic_search['resultats'][i]['experienceLibelle'])
        #base["altenance"].append(basic_search['resultats'][i]['altenance'])
        base["accessibleTH"].append(basic_search['resultats'][i]['accessibleTH'])
        base["origineOffre"].append(basic_search['resultats'][i]['origineOffre']['urlOrigine'])
    df = pd.DataFrame(base)
    df.to_csv(".../csv/offre.csv", index =False)
    return(df)

