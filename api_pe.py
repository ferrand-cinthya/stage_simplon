from offres_emploi import Api
import pandas as pd
import datetime
import re
import csv


def api_pe(*args):

    """La fonction requete l'API pole emploi afin d'obtenir les offres d'emploi disponible avec le mots clé data 
        Output : CSV des offres d'emploi disponible"""
    
    client = Api(client_id="PAR_suivirechechedestagec_19efb543c22da989b4d0a28181ebd3d92a94f02a49e6769b8d87608a1fcc8704", 
             client_secret="c09c885d8f49546c18a687df2ddd271f6e220f56cfceb0e00e359662e60c1ead")
    
    #list_mots_cles = ['data', 'données']
    
    data = {"intitule":[],
            "nom_entreprise": [],
            "lieu": [], "date": [],
            "lien":[],
            "description":[]
            }

    for word in args:
        
        params = {"motsCles": word,
                  "region": 84,
                  "sort" : 1
                 }
        basic_search = client.search(params=params)
    
        for i in range(len(basic_search['resultats'])):
            data["intitule"].append(basic_search['resultats'][i]['intitule'])
            
            lieu = basic_search['resultats'][i]['lieuTravail']['libelle']
            m = re.search(r'- (?P<lieu>\w+)', lieu)
            lieu = m.group(1)
            data["lieu"].append(lieu.lower())
            
            date = basic_search['resultats'][i]['dateCreation']
            date = datetime.datetime.strptime(date, ('%Y-%m-%dT%H:%M:%S.%f%z'))
            
            data["date"].append(date.date())
            data["lien"].append(basic_search['resultats'][i]['origineOffre']["urlOrigine"])
            data["description"].append(basic_search['resultats'][i]['description'])
        
            if "nom" in basic_search['resultats'][i]['entreprise']:
                data["nom_entreprise"].append(basic_search['resultats'][i]['entreprise']["nom"])
            else:
                data["nom_entreprise"].append("Unknow")
            

    df = pd.DataFrame(data)
    df.to_csv("/home/ferrand/projet/stage/stage_simplon/csv/offre.csv", index =False)
    return (df)

api_pe('informatique')
