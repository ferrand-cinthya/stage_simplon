from django.db import models
from django.db import models
from django.forms import ModelForm
from django.forms import formset_factory


class Entreprise(models.Model):
   nom = models.CharField(max_length=100)
   domaine = models.CharField(max_length=100)
   SIZE_ENT = [
      ('Start_up','Start_up'),
   	( 'Association','Association'),
   	( 'Micro_entreprise','Micro_entreprise'),
   	('Petit_entreprise','Petit_entreprise'),
   	('Moyenne_entreprise','Moyenne_entreprise'),
   	('Grande_entreprise','Grande_entreprise'),
   	('Multi_national','Multi_national'),
   	]
   taille = models.CharField(max_length=50,choices=SIZE_ENT)
   adresse= models.CharField(max_length=200)
   code_postal=models.CharField(max_length=5)
   ville=models.CharField(max_length=20)
   web = models.CharField(max_length=1000)
   def __str__(self):
        return f"{self.nom}"


class EntrepriseFrom(ModelForm):
    class Meta:
        model = Entreprise
        fields = '__all__'
def __str__(self):
        return f"{self.nom}"

class Contact(models.Model):
	entreprise = models.ForeignKey('Entreprise',on_delete=models.CASCADE)
	nom = models.CharField(max_length=100,unique=True)
	prenom = models.CharField(max_length=100)
	mail = models.EmailField(max_length=254)
	fixe =models.SlugField()
	mobile= models.SlugField()
	lieu=models.CharField(max_length=100)
def __str__(self):
        return f"{self.entreprise}"

class ContactFrom(ModelForm):
    class Meta:
        model = Contact
        fields = '__all__'
def __str__(self):
        return f"{self.entreprise}"