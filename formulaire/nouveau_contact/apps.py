from django.apps import AppConfig


class NouveauContactConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'nouveau_contact'
