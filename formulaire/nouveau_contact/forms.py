from django.forms import ModelForm
from .models import Entreprise, Contact
from django import forms
from django.db.models.query import QuerySet

class EntrepriseForm(ModelForm):
    class Meta:
      model = Entreprise
      fields ='__all__'

    def __init__(self, *args , **kwargs):
        super(EntrepriseForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
        })
        
    def str(self,QuerySet):
      return f"{self.nom} "  

class ContactForm(ModelForm):
    class Meta:
      model = Contact
      fields ='__all__'

    def __init__(self, *args , **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
        })
    def str(self,QuerySet):
      return f"{self.entreprise} "