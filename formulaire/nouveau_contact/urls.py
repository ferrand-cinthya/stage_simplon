from django.urls import path

from . import views

urlpatterns = [
    path('',views.home, name='home'),
    path('entreprise', views.entreprise, name='entreprise'),
    path('contact',views.contact, name='contact'),
    path('visu',views.ent, name='visu'),
    path('cont',views.cont, name='contact')
    ]