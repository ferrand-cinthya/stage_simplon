from django.shortcuts import render
from .forms import EntrepriseForm, ContactForm
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.db import models
from nouveau_contact.models import Entreprise, Contact
import csv
from django.db.models.query import QuerySet
            

@login_required
def home(request):
    return render(request, 'nouveau_contact/home.html')


def entreprise(request):
    if request.method == 'POST':
        form = EntrepriseForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
        return redirect('home',{'entreprise':entreprise})
    else:
        form = EntrepriseForm()
    return render(request, 'nouveau_contact/entreprise.html', {'form': form})

def ent(request):
    entre = Entreprise.objects.all()
    return render(request,'nouveau_contact/visu.html',{'ent': entre})

def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
        return redirect('home')
    else:
        form = ContactForm()
    return render(request, 'nouveau_contact/contact.html', {'form': form})


def cont(request):
    cont = Contact.objects.all()
    return render(request,'nouveau_contact/cont.html',{'cont': cont})
