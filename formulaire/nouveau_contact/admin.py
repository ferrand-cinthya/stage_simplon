from django.contrib import admin
from .models import Entreprise, Contact

admin.site.register(Entreprise)
admin.site.register(Contact)